<?php
/**
 * @file
 * Contains \Drupal\config_quickedit\Form\BlockForm.
 */


namespace Drupal\config_quickedit\Form;

use Drupal\block\BlockForm as CoreBlockForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Block Form for Quick Edit Context.
 */
class BlockForm extends CoreBlockForm {
  /**
   * {@inheritdoc}
   */
  protected function actionsElement(array $form, FormStateInterface $form_state) {
    $actions_element = parent::actionsElement($form, $form_state);
    $actions_element['submit']['#attributes']['class'][] = 'use-ajax-submit';
    return $actions_element;
  }

  /**
   * {@inheritdoc}
   */
  protected function buildVisibilityInterface(array $form, FormStateInterface $form_state) {
    $elements = parent::buildVisibilityInterface($form, $form_state);
    unset($elements['visibility_tabs']);
    $new_elements = [
      '#type' => 'details',
      '#title' => $this->t('Visibility Settings'),
    ];

    foreach ($elements as $key => $element) {
      unset($element['#group']);
      $new_elements[$key] = $element;
      unset($elements[$key]);
    }
    return $new_elements;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    // Do not allow redirect from parent because being submit via Ajax.
    $form_state->disableRedirect();
  }


}
